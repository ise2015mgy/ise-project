﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PL;
using BL;
using DAL;


namespace MainProgram
{
    class Program
    {
        static void Main(string[] args)
        {
            IDAL adal = new LINQ_DAL();
            IBL abl = new E_Mart_BL(adal);
            IPL apl = new PL_CLI(abl);

            apl.Run();
        }
    }
}
