﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;

namespace Backend
{
    [XmlRoot("E-Mart"), Serializable]
    public class Departments
    {
          private List<Department> departments = new List<Department>();

        /*********************Constructor************************/
        public Departments(List<Department> departments)
        {
            this.departments = departments;
        }
        public Departments()
        {

        }
        /**************Methods***********************/
        /*****get/set******/
        public List<Department> Departmentss
        {
            get { return this.departments; }
            set { this.departments = value; }
        }
        /******************************/
        public void addDepartment(Department departmentToAdd)
        {
            departments.Add(departmentToAdd);
        }
        public void removeDepartment(Department departmentToRemove)
        {
            departments.Remove(departmentToRemove);
        }
        /*************Other***************/
        public string toString()
        {
            string ans = "";
            foreach(Department department in departments)
            {
                ans = ans + department.toString() + "\n";
            }
            return ans;
        }
    }
    
}
