﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;

namespace Backend
{
    [XmlRoot("E-Mart"), Serializable]
    public class ProductSale
    {
        private int productID;
        private double price;

/***************************************Methods***************************************/
        /********************Constructor*****************************/
        public ProductSale(int productID, double price)
        {
            this.productID = productID;
            this.price = price;
        }
        public ProductSale()
        {

        }

        public ProductSale(Product product)
        {
            this.productID = product.InventoryID;
            this.price = product.Price;
        }
        public ProductSale(string productID , string price)
        {
            try
            {
                this.productID = Convert.ToInt32(productID);
            }
            catch (FormatException)
            {
                throw new Exception("The Product ID Must Be A Number. [Location: Employee.counstructor]");
            }
            catch (OverflowException)
            {
                throw new Exception("The Product ID is Either To Small Or To Big [Location: Employee.counstructor]");
            }
            try
            {
                this.price = Convert.ToDouble(price);
            }
            catch (FormatException)
            {
                throw new Exception("The Price Must Be A Number. [Location: Employee.counstructor]");
            }
            catch (OverflowException)
            {
                throw new Exception("The Price is Either To Small Or To Big [Location: Employee.counstructor]");
            }
        }
        /************get/set*************/
        public int ProductID
        {
            get { return this.productID; }
            set { this.productID = value; }
        }

        public double Price
        {
            get { return this.price; }
            set { this.price = value; }
        }
        /***************Other***************/
        public string toString()
        {
            return "product ID / price: " + productID.ToString() + " / " + price.ToString();
        }

    }
}
