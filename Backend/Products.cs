﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;

namespace Backend
{
    [XmlRoot("E-Mart"), Serializable]
    public class Products
    {
          private List<Product> products = new List<Product>();

        /*********************Constructor************************/
        public Products(List<Product> products)
        {
            this.products = products;
        }
        public Products()
        {

        }
        /**************Methods***********************/
        /*****Get/Set***********/
        public List<Product> Productss
        {
            get { return this.products; }
            set { this.products = value; }
        }
        public void addProduct(Product productToAdd)
        {
           products.Add(productToAdd);
        }
        public void removeProduct(Product productToRemove)
        {
            products.Remove(productToRemove);
        }

        public void editProduct(Product p)
        {
            for (int i = 0; i < products.Count;i++ )
            {
                if(products.ElementAt(i).InventoryID==p.InventoryID)
                {
                    removeProduct(products.ElementAt(i));
                    addProduct(p);
                    return;
                }
            }
        }
        /*************Other***************/
        public string toString()
        {
            string ans = "";
            foreach(Product product in products)
            {
                ans = ans + product.toString() + "\n";
            }
            return ans;
        }
    }
    
}
