﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;

namespace Backend
{
    [XmlRoot("E-Mart"), Serializable]
    public class ClubMembers
    {
          private List<ClubMember> clubMembers = new List<ClubMember>();

        /*********************Constructor************************/
        public ClubMembers(List<ClubMember> clubMembers)
        {
            this.clubMembers = clubMembers;
        }
        public ClubMembers()
        {

        }
        /**************Methods***********************/
        /**********get/set**********************/
        public List<ClubMember> ClubMemberss
        {
            get { return this.clubMembers; }
            set { this.clubMembers = value; }
        }
        public void addClubMember(ClubMember clubMemberToAdd)
        {
            clubMembers.Add(clubMemberToAdd);
        }
        public void removeClubMember(ClubMember clubMemberToRemove)
        {
            clubMembers.Remove(clubMemberToRemove);
        }
        /*************Other***************/
        public string toString()
        {
            string ans = "";
            foreach(ClubMember clubMember in clubMembers)
            {
                ans = ans + clubMember.toString() + "\n";
            }
            return ans;
        }
    }
    
}
