﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;

namespace Backend
{
    [XmlRoot("E-Mart"), Serializable]
    public class ClubMember
    {
        private int memberID;
        private int teudatZehute;
        private string firstName;
        private string lastName;
        private List<string> transactionHistory = new List<string>();////?
        private Gender gender;
        private DateTime date_of_birth;////?

        /************************************************Methods*********************************************/
        /************counstructor****************/
        public ClubMember() { }

        public ClubMember(int memberID, int teudatZehute, string firstName,string lastName,List<string> transactionHistory,string geender,DateTime date_of_birth)
        {
            this.memberID = memberID;
            this.teudatZehute = teudatZehute;
            this.firstName = firstName;
            this.lastName = lastName;
            this.transactionHistory = transactionHistory;
            this.date_of_birth = date_of_birth;
            this.gender = (Gender)Enum.Parse(typeof(Gender), geender);
        }

        public ClubMember(ClubMember clubmember)
        {
            this.memberID = clubmember.memberID;
            this.teudatZehute = clubmember.teudatZehute;
            this.firstName = clubmember.firstName;
            this.lastName = clubmember.lastName;
            this.transactionHistory = clubmember.transactionHistory;
            this.date_of_birth = clubmember.date_of_birth;
            this.gender = clubmember.gender;
        }

        public ClubMember(string teudatZehute, string firstName, string lastName, string geender, string date_of_birth)
        {
            this.firstName = firstName;
            this.lastName = lastName;

            try
            {
                this.teudatZehute = Convert.ToInt32(teudatZehute);
            }
            catch (FormatException)
            {
                throw new Exception("The Teudat Zehute Must Be A Number. [Location: Employee.counstructor]");
            }
            catch (OverflowException)
            {
                throw new Exception("The Teudat Zehute is Either To Small Or To Big [Location: Employee.counstructor]");
            }
            try
            {
                this.gender = (Gender)Enum.Parse(typeof(Gender), geender);
            }
            catch (ArgumentException)
            {
                throw new Exception("The Gender You Entered Doesnt exist. [Location: Employee.counstructor]");
            }
            try
            {
                this.date_of_birth = Convert.ToDateTime(date_of_birth); 
            }
            catch (FormatException)
            {
                throw new Exception("Your date of birth must be in the following order ");
            }


        }

        /****************get / set******************/
        public int MemberID
        {
            get { return this.memberID; }
            set { this.memberID = value; }
        }
        public int TeudatZehute
        {
            get { return this.teudatZehute; }
            set { this.teudatZehute = value; }
        }
       
        public string FirstName
        {
            get { return this.firstName; }
            set { this.firstName = value; }
        }

        public string LastName
        {
            get { return this.lastName; }
            set { this.lastName = value; }
        }

        public string Gender
        {
            get { return this.gender.ToString(); }
        }

        public DateTime Date_of_birth
        {
            get { return this.date_of_birth; }
            set { this.date_of_birth = value; }
        }
        public List<string> TransactionHistory
        {
            get { return this.transactionHistory; }
            set { this.transactionHistory = value; }
        }
        public void addTransactionToHistory(string toAdd)
        {
            transactionHistory.Add(toAdd);
        }
        
        public string getAllTransactionsFromHistory()
        {
            string ans = "";
            foreach(string s in transactionHistory)
            {
                ans = ans + s + " ";
            }
            return ans;
        }

        /*********************Other************************/
        public string toString()
        {
            return "member Id: " + memberID.ToString() +
                    " teudat zehute: " + teudatZehute.ToString() +
                    " full name: " + firstName + " " + lastName +
                    " transaction history : " + getAllTransactionsFromHistory() +
                    " gender: " + gender.ToString() +
                    " date of birth: " + date_of_birth.ToString();/////////?????
        }

     }
}
