﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;

namespace Backend
{
    [XmlRoot("E-Mart"), Serializable]
    public class User
    {
        /**********************Fields****************************/
        private string userName;
        private string password;
        /**********************Methods**********************************/
        /***************************************Constructor*************************************/
        public User(string userName,string password)
        {
            this.userName = userName;
            this.password = password;
        }

        public User(User user)
        {
            this.UserName = user.UserName;
            this.Password = user.Password;
        }

        public User()
        {

        }
            /***********gETTERS/sETTERS**************/
        public string UserName
        {
            get { return this.userName; }
            set { this.userName = value; }
        }
        public string Password
        {
            get { return this.password; }
            set { this.password = value; }
        }

        /**********************Other******************/
        public string toString()
        {
            return "User Name: " + userName +
                   " Password: " + password;
        }
    }
}
