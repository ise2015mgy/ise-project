﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Backend;

namespace DAL
{
    public class Product_LINQ_DAL
    {
        public List<Product> products;

        public Product_LINQ_DAL(Products products)
        {
            this.products = products.Productss;
        }

        public void addProduct(Product p)
        {
            this.products.Add(p);
        }
        public void removeProduct(Product p)
        {
            this.products.Remove(p);
        }
        public void editProduct(Product p)
        {
            for (int i = 0; i < products.Count; i++)
            {
                if (products.ElementAt(i).InventoryID == p.InventoryID)
                {
                    removeProduct(products.ElementAt(i));
                    addProduct(p);
                    return;
                }
            }
            
        }
    }
}
