﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Backend;

namespace DAL
{
    public interface IDAL
    {
        void addProduct(Product p);
        void removeProduct(Product p);
        void editProduct(Product p);
        void addDepartment(Department d);
        void removeDepartment(Department d);
        void editDepartment(Department d);
        void addUser(User p);
        void removeUser(User p);
        void editUser(User p);
        void addEmployee(Employee e);
        void removeEmployee(Employee e);
        void editEmployee(Employee e);

    }
}
