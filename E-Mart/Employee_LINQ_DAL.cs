﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Backend;

namespace DAL
{
    public class Employee_LINQ_DAL
    {
    private List<Employee> employees;

    public Employee_LINQ_DAL(Employees employees)
        {
            this.employees = employees.Employeess;
        }

        public void addEmployee(Employee e)
        {
            employees.Add(e);
        }

        public void removeEmployee(Employee e)
        {
            employees.Remove(e);
        }

        public void editEmployee(Employee e)
        {
            for (int i = 0; i < employees.Count; i++)
            {
                if (employees.ElementAt(i).TeudatZehute == e.TeudatZehute)
                {
                    removeEmployee(employees.ElementAt(i));
                    addEmployee(e);
                    return;
                }
            }
        }
    }
}
