﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.Xml;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using Backend;

namespace DAL
{

    public class LINQ_DAL : IDAL
    {
        private string filename = "C:\\Users\\SHELDON\\Desktop\\myDB.xml";
        string password = "MAKV2SPBNI99212";
        Product_LINQ_DAL product_Linq_DAL;
        Employee_LINQ_DAL employee_Linq_DAL;
        ClubMember_LINQ_DAL clubMember_Linq_DAL;
        Department_LINQ_DAL department_Linq_DAL;
        Transaction_LINQ_DAL transaction_Linq_DAL;
        User_LINQ_DAL user_Linq_DAL;
                
        private Serialize serialize = new Serialize();
        private AES aes = new AES();
        Backend.E_Mart_Store eMart;
        public LINQ_DAL()
        {
            if (File.Exists(filename))
            {
                this.eMart = FromFile(filename, password);
                this.product_Linq_DAL = new Product_LINQ_DAL(eMart.Products);
                this.employee_Linq_DAL = new Employee_LINQ_DAL(eMart.Employees);
                this.clubMember_Linq_DAL = new ClubMember_LINQ_DAL(eMart.Clubmembers);
                this.department_Linq_DAL = new Department_LINQ_DAL(eMart.Departments);
                this.transaction_Linq_DAL = new Transaction_LINQ_DAL(eMart.Transactions);
                this.user_Linq_DAL = new User_LINQ_DAL(eMart.Users);
            }
            else
                eMart = null;
            //ToFile(eMart, password, "C:\\Users\\SHELDON\\Desktop\\myDB2.xml");
            //Console.Write(eMart.toString());
            //Console.Read();
        }

        public void toConsole(E_Mart_Store db)
        {

            XmlSerializer x1 = new XmlSerializer(typeof(E_Mart_Store));
            x1.Serialize(Console.Out, db);
            Console.WriteLine();
            Console.ReadLine();

        }

        public void ToFile(E_Mart_Store p, string password, string file)
        {
            byte[] passwordBytes = StrToByte(password);

            byte[] bytes = serialize.SerializeObjectToByteArray(p);

            byte[] encryptedBytes = aes.AES_Encrypt(bytes, passwordBytes);

            ByteToFile(encryptedBytes, file);

        }

        public E_Mart_Store FromFile(string file, string password)
        {


            byte[] passwordBytes = StrToByte(password);

            byte[] fileInBytes = FileToByte(file);

            byte[] decryptedFileInBytes = aes.AES_Decrypt(fileInBytes, passwordBytes);

            E_Mart_Store p = serialize.DeserializeByteArrayToObject(decryptedFileInBytes);

            return p;
        }

        public byte[] StrToByte(string str)
        {
            byte[] bytes = new byte[str.Length * sizeof(char)];
            System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public byte[] FileToByte(string filename)
        {
            byte[] bytes = System.IO.File.ReadAllBytes(filename);
            return bytes;

        }

        public void ByteToFile(byte[] bytes, string filename)
        {
            File.WriteAllBytes(filename, bytes);
        }

        public void addProduct(Product p)
        {
            this.product_Linq_DAL.addProduct(p);
        }

        public void removeProduct(Product p)
        {
            this.product_Linq_DAL.removeProduct(p);
        }
        public void editProduct(Product p)
        {
            this.product_Linq_DAL.editProduct(p); 
        }
        public void addDepartment(Department d)
        {
            this.department_Linq_DAL.addDepartment(d);
        }
        public void removeDepartment(Department d)
        {
            this.department_Linq_DAL.removeDepartment(d);
        }
        public void editDepartment(Department d)
        {
            this.department_Linq_DAL.editDepartment(d);
        }

        public void addUser(User u)
        {
            this.user_Linq_DAL.addUser(u);
        }

        public void removeUser(User u)
        {
            this.user_Linq_DAL.removeUser(u);
        }

        public void editUser(User u)
        {
            this.user_Linq_DAL.editUser(u);
        }

        public void addEmployee(Employee e)
        {
            employee_Linq_DAL.addEmployee(e);
        }

        public void removeEmployee(Employee e)
        {
            employee_Linq_DAL.removeEmployee(e);
        }

        public void editEmployee(Employee e)
        {
            employee_Linq_DAL.editEmployee(e);
        }
    }
}
