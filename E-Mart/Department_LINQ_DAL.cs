﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Backend;

namespace DAL
{
    public class Department_LINQ_DAL
    {
        private List<Department> departments;

    public Department_LINQ_DAL(Departments departments)
        {
            this.departments = departments.Departmentss;
        }

        public void addDepartment(Department d)
        {
            departments.Add(d);
        }

        public void removeDepartment(Department d)
        {
            departments.Remove(d);
        }

        public void editDepartment(Department d)
        {
            
                for (int i = 0; i < departments.Count; i++)
                {
                    if (departments.ElementAt(i).DepartmentID == d.DepartmentID)
                    {
                        removeDepartment(departments.ElementAt(i));
                        addDepartment(d);
                        return;
                    }
                }

            
        }
    }
}
