﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend;
using DAL;

namespace BL
{
    public class Product_BL
    {
        IDAL itsDAL;

        public Product_BL(IDAL itsDAL)
        {
            this.itsDAL = itsDAL;
        }

        /***************************** Add Product *******************************/
        public void addProduct(Product p)
        {
            try
            {
                setInventoryId(p);
                validateDepartmentID(p);
                setInStock(p);
            }catch(Exception e)
            {
                throw e;
            }
          
            itsDAL.addProduct(p);
        }
      /******************************* Remove Product **************************************/

        public void removeProduct(Product p)
        {
            itsDAL.removeProduct(p);
        }

        /******************************** Edit Product *****************************************/
        public void editProduct(Product p)
        {
            try
            {
                validateDepartmentID(p);
                setInStock(p);
            }
            catch (Exception e)
            {
                throw e;
            }
            itsDAL.editProduct(p);
        }



        public void setInventoryId(Product p)
        {
            Products allProducts = itsDAL.getAllProducts();
            int maxID = 0;
            foreach (Product prod in allProducts.Productss)
            {
                if (prod.InventoryID > maxID)
                {
                    maxID = prod.InventoryID;
                }
            }
            p.InventoryID = maxID++;
        }
        public void validateDepartmentID(Product p)
        {
            Departments allDepartments = itsDAL.getAllDepartments();
            bool found = false;
            foreach (Department dep in allDepartments.Departmentss)
            {
                if (dep.DepartmentID == p.Location)
                {
                    found = true;
                    break;
                }

            }
            if (!found)
            {
                throw new Exception("Adding product failed: Department: " + p.Location + " Not Found [Location: Product_BL.addProduct()]");
            }
        }
        public void setInStock(Product p)
        {
            if (p.StockCount == 0)
            {
                p.INStock = InStock.False;

            }
            else if (p.StockCount <= p.WhenToOrder)
            {
                p.INStock = InStock.NeedToOrder;
            }
            else p.INStock = InStock.True;
        }
   
    }
}
