﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend;
using DAL;


namespace BL
{
    public class E_Mart_BL: IBL
    {
        private IDAL itsDAL;
        private Product_BL prod_bl;
        private Employee_BL employee_bl;
        private ClubMember_BL clubMember_bl;
        private Department_BL department_bl;
        private Transaction_BL transaction_bl;
        private User_BL user_bl;

        public E_Mart_BL(IDAL dal)
        {
            this.itsDAL = dal;
            this.prod_bl = new Product_BL(dal);
            this.employee_bl = new Employee_BL(dal);
            this.clubMember_bl = new ClubMember_BL(dal);
            this.department_bl = new Department_BL(dal);
            this.transaction_bl = new Transaction_BL(dal);
            this.user_bl = new User_BL(dal);
        }
        public E_Mart_BL() { }




        public void add(object obj)
        {
            if(obj is Product){ prod_bl.addProduct((Product)obj); }
            else if(obj is Employee){ employee_bl.addEmployee((Employee)obj); }
            else if(obj is ClubMember){ clubMember_bl.addClubMember((ClubMember)obj); }
            else if(obj is Department){ department_bl.addDepartment((Department)obj); }
            else if(obj is Transaction){ transaction_bl.addTransaction((Transaction)obj); }
            else if(obj is User){ user_bl.addUser((User)obj); }
            else
              {
                  throw new Exception("You Are Trying To Add An Unfamiliar Object [Location: BL.E_Mart_BL]");
              }
        }

        public void remove(object obj)
        {
            if (obj is Product) { prod_bl.removeProduct((Product)obj);}
            else if (obj is Employee) { employee_bl.removeEmployee((Employee)obj); } 
            else if (obj is ClubMember){ clubMember_bl.removeClubMember((ClubMember)obj); }
            else if (obj is Department){ department_bl.removeDepartment((Department)obj); }
            else if (obj is Transaction) { transaction_bl.removeTransaction((Transaction)obj); }
            else if (obj is User) { user_bl.removeUser((User)obj); }
            else
               {
                  throw new Exception("You Are Trying To Remove An Unfamiliar Object [Location: BL.E_Mart_BL]");
               }
        }

        public void edit(object obj)
        {
            if (obj is Product) { prod_bl.editProduct((Product)obj); }
            else if (obj is Employee) { employee_bl.editEmployee((Employee)obj); }
            else if (obj is ClubMember) { clubMember_bl.editClubMember((ClubMember)obj); }
            else if (obj is Department) { department_bl.editDepartment((Department)obj); }
            else if (obj is Transaction) { transaction_bl.editTransaction((Transaction)obj); }
            else if (obj is User) { user_bl.editUser((User)obj); }
            else
            {
                throw new Exception("You Are Trying To Edit An Unfamiliar Object [Location: BL.E_Mart_BL]");
            }
        }
    }
}
