﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using Backend;

namespace BL
{
    public class User_BL
    {
        IDAL itsDAL;

        public User_BL(IDAL dal)
        {
            this.itsDAL = dal;
        }

        public void addUser(User user)
        {
            doesNameExist(user);
            this.itsDAL.addUser(user);
        }

        public void editUser(User user)
        {
            this.itsDAL.editUser(user);
        }

        public void removeUser(User user)
        {
            this.itsDAL.removeUser(user);
        }

        public void doesNameExist(User user)
        {
            Users allUsers = this.itsDAL.getAllUsers();
            foreach (User user1 in allUsers.Userss)
            {
                if (user1.UserName.Equals(user.UserName))
                {
                    throw new Exception("User name is in use");
                } 
            }
        }

    }
}
