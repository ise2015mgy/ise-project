﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using Backend;

namespace BL
{
    class Employee_BL
    {
         IDAL itsDAL;

        public Employee_BL(IDAL itsDAL)
        {
            this.itsDAL = itsDAL;
        }

        public void addEmployee(Employee employee)
        {
            try
            {
                validateDepartmentID(employee.DepartmentID);
                validateSupervisorID(employee.SupervisorID);
            }
            catch(Exception e )
                {
                    throw e;
                }
            itsDAL.addEmployee(employee);
        }
        
        public void removeEmployee(Employee employee)
        {
            try
            {
                isSupervisor(employee.TeudatZehute);
            }
            catch(Exception e)
            {
                throw e;
            }
        }

        public void editEmployee(Employee employee)
        {
            try
            {
                validateDepartmentID(employee.DepartmentID);
                validateSupervisorID(employee.SupervisorID);
            }
            catch (Exception e)
            {
                throw e;
            }
            itsDAL.editEmployee(employee);
        }

        private void validateDepartmentID(int departmentID)
        {
            Departments allDepartments = itsDAL.getAllDepartments();
            foreach (Department dep in allDepartments.Departmentss)
            {
                if (dep.DepartmentID == departmentID)
                {
                    throw new Exception("Adding Employee failed: Department: " + departmentID + " Not Found [Location: Employee_BL.addEmployee()]");
                }
            }
        }
        private void validateSupervisorID(int supervisorID)
        {
            Employees allEmployees = itsDAL.getAllEmployees();
            foreach(Employee employee in allEmployees.Employeess)
            {
                if(employee.TeudatZehute == supervisorID)
                {
                    throw new Exception("Adding Employee Failed: Supervisor: " + supervisorID + " Not Found [Location: Employee_BL.addEmployee()]");
                }
            }
        }

        private void isSupervisor(int employeeID)
        {
            Employees allEmployees = itsDAL.getAllEmployees();
            foreach (Employee employee in allEmployees.Employeess)
            {
                if (employee.SupervisorID == employeeID)
                {
                    throw new Exception("Deleting Employee Failed: Employee is a Supervisor first manage his staff [Location: Employee_BL.removeEmployee()]");
                }
            }
        }
    }
}
