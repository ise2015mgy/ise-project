﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend;
using DAL;

namespace BL
{
    public class Department_BL
    {
      
            private IDAL itsDAL;

            public Department_BL(IDAL dal)
            {
                this.itsDAL = dal;
            }
        
        /***************************** Add Department *******************************/
            public void addDepartment(Department d)
        {

            setDepartmentID(d);
            itsDAL.addDepartment(d);
            
        }
        /******************************* Remove Department **************************************/

        public void removeDepartment(Department d)
        {
           
                try { AssignedProducts(d); }
                catch (Exception e) { throw e; }
                itsDAL.removeDepartment(d);
            
        }

        /******************************** Edit Department *****************************************/
        public void editDepartment(Department d)
        {
            
               itsDAL.editDepartment(d);
          
        }
        /*****************************************************************************/
        public void AssignedProducts(Department d)
        {
            Products allProducts = itsDAL.getAllProducts();
            bool found = false;
            foreach (Product prod in allProducts.Productss)
            {
                if (prod.Location == d.DepartmentID)
                {
                    throw new Exception("You are tring to remove a department that have assigned products to, please clear all products from the department first ");
                }

            }
        }
        public void setDepartmentID(Department d)
        {
            Departments allDepartments = itsDAL.getAllDepartments();
            int maxID = 0;
            foreach (Department prod in allDepartments.Departmentss)
            {
                if (prod.DepartmentID > maxID)
                {
                    maxID = prod.DepartmentID;
                }
            }
            d.DepartmentID = maxID++;
        }

    }
}
