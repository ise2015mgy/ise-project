﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend;
using DAL;

namespace BL
{
    public interface IBL
    {
        void add(object obj);
        void remove(object obj);
        void edit(object obj);

        //Products findProductByName(string name);
    }
}
