﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Backend;
using BL;

namespace PL
{
    public class PL_CLI: IPL
    {
        private IBL itsBL;
    
        public PL_CLI(IBL BL)
        {
            this.itsBL = BL;
        }

        private void displayResult(List<Product> prods)
        {
            foreach(Product p in prods)
            {
                Console.WriteLine(p.Name + " " + p.Type.ToString());
            }
        }

        private string receiveCmd()
        {
            ///////
            return Console.ReadLine();
        }

        public void Run()
        {
            string cmd;
            while(true)
            {
                cmd = receiveCmd();
                
            }
        }


    
    }
    
}
